const words = [
    'ENGLAND','GERMANY','BRAZIL','JAPAN','DENMARK','FRANCE',
    'INDIA','ISRAEL','JAMAICA','MEXICO','PALESTINE','RUSSIA',
    'SWEDEN','CUBA','VIETNAN','CHILE','CANADA','CHINA','HAITI',
    'GREECE','IRAN','ITALY','ECUADOR','EGYPT','LYBIA','CROATIA'
];
const letters = [
    'A','B','C','D','E','F','G','H','I','J','K','L','M',
    'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
];
let board = [
    [],[],[],[],[],[],[],[],[],[],[],[],[],
    [],[],[],[],[],[],[]
];
let winCondition = {id: [], text: ''};
let wordsChoose = [];
const tabela = document.getElementById('tabela');
const btnCreate = document.getElementById('btnCreate');
const btnReset = document.getElementById('btnReset');
const palavras = document.getElementById('palavras');
const tutorial = document.getElementById('tutorial');
const selected = 'selected';
const deactivate = 'deactivate';

const randomNum = (min,max) => {
    return Math.floor(Math.random() * (max - min)) + min;
}
const resetWinCondition = (obj) =>{

    
    for (let count = 0; count < obj.id.length; count++) {

        // Gambiarra (arrumar dps)
        if (obj.id[count] === '') {
            continue;
        }
        
        let elem = document.getElementById(obj.id[count]);
        elem.classList.remove(selected);

    }

    winCondition = {id:[], text: ''};
}
const addÐeactivate = (obj) => {
    for (let count = 0; count < obj.id.length; count++) {

        let elem = document.getElementById(obj.id[count]);
        elem.classList.add(deactivate);

    }
     
    resetWinCondition(obj);
}
const legendaPalavras = (array) => {

    for(let index = 0; index < array.length; index++){
        
        let newP = document.createElement('p');
        let palavra = array[index];
        newP.classList.add('legenda');
        newP.innerHTML = palavra;
        palavras.appendChild(newP);
        
    }
}

const finishingGame = () =>{

    alert(`Parabéns você achou todas as palavras!`);
    const legenda = document.querySelectorAll('.legenda')
    const tds = document.querySelectorAll('.letter');

    for(let count = 0; count < tds.length; count++) {

        tds[count].remove();
         
    }
    for(let index = 0; index < legenda.length; index++){

        legenda[index].remove();

    }
    
    
    winCondition = {id:[], text: ''};

    btnCreate.disabled = false;
    btnCreate.classList.remove(deactivate);
    tutorial.classList.remove('hidden')

}
const toNumbers = (x) => {
    return parseInt(x,10);
}
const smallestNumber = (array) => {
    return array.reduce((x,y) => Math.min(x,y));
}
const analizeIDs = (obj) =>{
    let objArray = obj.id.map(toNumbers);

    let numero = 0;
    let numero2 = 0;
    let numero3 = 0;

    for (let index = 0; index < objArray.length; index++) {

        numero += objArray[index];
        numero2 = smallestNumber(objArray) * objArray.length;
        numero3 = numero3 + (index)
      
    }
    if (numero - numero2 === numero3) {
        return true;
    }

    return false;
}
const removeWordCorrect = (word) => {
    let index = wordsChoose.indexOf(word);
    wordsChoose.splice(index, 1);

    if(wordsChoose.length === 0){
        
        finishingGame()
        
    }
}
const fillBoard = () => {
    for(let index = 0; index < board.length; index++){

        let row = board[index];

        for(let i = 0; i < 10; i++){

            row[i] = letters[randomNum(words.length,0)];
        
        }
    }
    
}
const chooseWords = () => {
    wordsCopy = [...words];

    for (let i = 0; i < 3; i++) {
        let num = randomNum(0, wordsCopy.length);
        wordsChoose.push(wordsCopy[num]);
        wordsCopy.splice(num, 1);
    }


}
const addWordsBoard = (array) =>{

    for(let count = 0; count < array.length; count++){
        
        let num = randomNum(21,0);
        console.log(`${num + 5} <-- NÃO pode repetir`);
        let num2 = randomNum(11,0);
       
        let position = board[num];
        
        let word = array[count];

        if(num2 + word.length > 10){

            let extra = (num2 + word.length) - 10;
            num2 = num2 - extra;

        }

        for(let index = num2; index < position.length; index++){
            
            for(let j = 0; j < word.length; j++){
                
                position[num2 + j] = word[j].toUpperCase();
            }
           
        }
    }
    
}
const addBoardHTML = () => {

    const row = document.querySelectorAll('.row');
    let count = 0;

    for(let index = 0; index < board.length; index++){

        let actualRow = row[index];

        for(let indexCol = 0; indexCol < board[index].length ; indexCol++){

            let newTd = document.createElement('td');
            newTd.innerHTML = board[index][indexCol];
            newTd.classList.add('letter');
            newTd.setAttribute('id',`${count}`);
            actualRow.appendChild(newTd);
            count++;

        }
    }
}
const analizeWords = (word) => {
    
    for (let index = 0; index < wordsChoose.length; index++) {
        let time = 0;
        let atual = wordsChoose[index]
        for (let count = 0; count < word.length; count++) {

            if (word.length === atual.length) {

                if (atual.includes(word[count])) {
                    time++;
                    if(time === atual.length){
                        return atual;
                    }
                    continue;
                }
                else {
                    break;
                }
            }
            else {
                break;
            }
        }
    }
    return false;
}
const selectionLetters = (evt) => {
    
    evt.target.classList.add(selected);

    winCondition.id.push(evt.target.id);

    winCondition.text += evt.target.outerText
       
    if ( analizeIDs(winCondition)) {
        let actualWord = analizeWords(winCondition.text);
        if(wordsChoose.includes(actualWord)){
            alert(`Você achou a palavra ${actualWord}!`);
            removeWordCorrect(actualWord);
            addÐeactivate(winCondition);
        }
       
        
    }

    
}
const init = () => {
    fillBoard();
    chooseWords();
    addWordsBoard(wordsChoose);
    legendaPalavras(wordsChoose);
    addBoardHTML();
    tutorial.classList.add('hidden');
    btnCreate.disabled = true;
    btnCreate.classList.add(deactivate);
}



tabela.addEventListener('click', selectionLetters)

btnReset.addEventListener('click', function() {
    resetWinCondition(winCondition)
});
btnCreate.addEventListener('click', init);

// for(let i = 0 ; i < 100; i++){
//     let num = randomNum(21,0)
//     if(num === 0){
//         console.log('tem 0');
//     }
//     else if(num === 21){
//         console.log('tem 21')
//     }
//     else if (num === 10){
//         console.log('tem 10!')
//     }
// }